﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{

    public float ammount;
    public enum TypeItem
    {
        life,
        ammunition

    }

    public TypeItem typeItem;

    public Item() {
        float i = Random.Range(0f, 1f);
        if (i > 0.5f) {
            typeItem = TypeItem.ammunition;
            ammount = 5f;
        }
        else {
            typeItem = TypeItem.life;
            ammount = 10f;
        }
    }



    public void UseItem(Player p) {

        switch (typeItem) {

            case TypeItem.ammunition:
                p.ammunition = this.ammount;
                break;

            case TypeItem.life:
                p.life += this.ammount;
                break;

        }

        Destroy(this.gameObject);
    }
}
