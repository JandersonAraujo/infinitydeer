﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{

    public GameObject warriorPrefab;
    public Transform[] positionsToSpaw;
    public float timeToStartGame;

    float currentTime;
    public float timeToSpawEnemies;

    public bool paused = false;

    public bool canPlay;

    public GameObject PauseCanvas;

    void Start()
    {
        StartCoroutine("SpawMonster");
        ResumeGame();
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (paused)
            {
                ResumeGame();
            }
            else {
                PauseGame();
            }
        }

        currentTime += Time.deltaTime * 1f;

        if (currentTime >= timeToSpawEnemies)
        {
            currentTime = 0;
            if (canPlay) {

                StartCoroutine(SpawMonster());
            }
        }
    }

    IEnumerator SpawMonster() {

        Debug.Log("Esperei "+ timeToSpawEnemies + "segundos");

        int currentPosition = Random.Range(0, positionsToSpaw.Length);
        Instantiate(warriorPrefab, positionsToSpaw[currentPosition].position, positionsToSpaw[currentPosition].rotation);
        Debug.Log("instanciei um inimigo");

        yield return new WaitForSeconds(1f);

    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        PauseCanvas.SetActive(true);
    }

    public void ResumeGame() {
        Time.timeScale = 1f;
        PauseCanvas.SetActive(false);
    }

    public void ReturnMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadSceneAsync(0);

    }

    public void QuitGame() {
        Application.Quit();
    }
}
