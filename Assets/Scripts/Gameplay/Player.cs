﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Base
{
    public float life;
    public float ammunition;

    public Transform arrowPosition;
    public GameObject arrow;

    public Text textLife;
    public Text textAmmunition;

    private Player player;

    void Start()
    {
        player = GetComponent<Player>();
    }
    void Update()
    {
        Atack();

        textLife.text = "Life: " + life;
        textLife.text = "Amunition: " + ammunition;
    }

    new void Atack() {
        if (Input.GetButtonDown("Fire1"))
        {
            if (this.ammunition > 0) {
                GameObject _arrow =  Instantiate(arrow, arrowPosition.position, arrowPosition.rotation) as GameObject;
                ArrowBehavior b = _arrow.GetComponent<ArrowBehavior>();
                b.Damage = 10f;
                this.ammunition--;
            }
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "item") {
            Item i = other.GetComponent<Item>();
            i.UseItem(player);
        }
    }

}


