﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Melee : Base
{
    

    public Melee() {
        this.life = 10f;
        this.atackFireRate = 1f;
        
    }

    private new void Start()
    {
        this.navMeshAgent = GetComponent<NavMeshAgent>();
        this.animator = GetComponent<Animator>();
        this.target = GameObject.FindGameObjectWithTag("Player").transform;
        this.navMeshAgent.destination = this.target.position;
        this.currentState = StatesEnemies.chasing;
        skinnedMeshRenderer = gameObject.transform.GetComponentInChildren<SkinnedMeshRenderer>();
        this.SetColor();
    }

    private void Update()
    {
        this.Move();

    }

    private new void Move() {
        Chasing();
        Atack();
    }

    private void Chasing() {
        if (this.currentState == StatesEnemies.chasing)
        {
            this.navMeshAgent.destination = this.target.position;
            this.animator.SetBool("run", true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") {
            this.animator.SetBool("run", false);
            this.currentState = StatesEnemies.atack;
            this.navMeshAgent.destination = this.transform.position;
        }

        if (other.tag == "Arrow") {
            float damage = other.GetComponent<ArrowBehavior>().Damage;
            Debug.Log("Tomei Damage" + damage);
            this.GetDamage(damage);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Arrow") {

            float damage = collision.gameObject.GetComponent<ArrowBehavior>().Damage;
            Debug.Log("Tomei Damage" + damage);
            this.GetDamage(damage);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            this.currentState = StatesEnemies.chasing;
            this.animator.SetBool("atack", false);
        }
    }

    private void Atack() {

        if (this.currentState == StatesEnemies.atack)
        {
            this.currentAtackFireRate += 1 * Time.deltaTime;
            if (this.currentAtackFireRate >= this.atackFireRate) {
                
                StartCoroutine(CoroutineAtack());
            }
        }
    }

    private IEnumerator CoroutineAtack() {
        currentAtackFireRate = 0;
        this.animator.SetBool("atack", true);
        yield return new WaitForSeconds(1.5f);

    }

    private new void SetColor() {
        float currentColor = Random.RandomRange(0, 6);
        int _currentColor = Mathf.RoundToInt(currentColor);

        switch (_currentColor)
        {

            case 1:
                this.colorEnemy = ColorEnemy.azul;
                break;
            case 2:
                this.colorEnemy = ColorEnemy.green;
                break;
            case 3:
                this.colorEnemy = ColorEnemy.purple;
                break;
            case 4:
                this.colorEnemy = ColorEnemy.red;
                break;
            case 5:
                this.colorEnemy = ColorEnemy.teal;
                break;
            case 6:
                this.colorEnemy = ColorEnemy.yellow;
                break;

            default:
                break;
        }
        skinnedMeshRenderer.material = materials[_currentColor];
    }
}
