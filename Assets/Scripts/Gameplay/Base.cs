﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Base : MonoBehaviour
{

    protected float life;
    protected float atackFireRate;
    protected float currentAtackFireRate;
    protected Animator animator;
    protected StatesEnemies currentState;
    protected ColorEnemy colorEnemy;
    protected SkinnedMeshRenderer skinnedMeshRenderer;

    protected Transform target;
    protected NavMeshAgent navMeshAgent;
    public Material[] materials;
    

    protected enum StatesEnemies {
        idle,
        chasing,
        atack,
        die
    }

    protected enum ColorEnemy
    {
        azul,
        green,
        purple,
        red,
        teal,
        yellow
    }
    

    protected void Start()
    {
        animator = GetComponent<Animator>();
        currentState = StatesEnemies.idle;

    }

    protected void Atack() {

    }

    public void GetDamage(float damage) {
        this.life -= damage;
        if (this.life <= 0) {
            this.Die();
        }
    }

    protected void Die() {
        animator.SetBool("die", true);
        navMeshAgent.destination = this.transform.position;
    }

    protected void Move() {

    }

    protected void SetColor() {

    }
}
